const Login = () => import("@/components/auth/Login.vue")
const Registration = () => import("@/components/auth/Registration.vue")
const MyGreeting = () => import("@/components/greet/MyGreeting.vue")
const AdvertList = () => import("@/components/search/advert/AdvertList.vue")
const JobAdvertInstance = () => import("@/components/search/advert/JobAdvertInstance.vue")
const PersonalPage = () => import("@/components/home/PersonalPage.vue")
const MyAdverts = () => import("@/components/home/advert/MyAdverts.vue")
const CreateAdvert = () => import("@/components/home/advert/CreateAdvert.vue")
const MyProfiles = () => import("@/components/home/expert/MyProfiles.vue")
const CreateProfile = () => import("@/components/home/expert/CreateProfile.vue")
const MyReplies = () => import("@/components/home/reply/MyReplies.vue")
const EditingAdvert = () => import("@/components/home/advert/EditingAdvert.vue")

const routes = [
    {
        path: "/",
        component: MyGreeting,
        meta: { needAuth: false }
    },
    {
        path: "/adverts",
        component: AdvertList,
        meta: { needAuth: false },
    },
    {
        path: "/adverts/:id",
        component: JobAdvertInstance,
        meta: { needAuth: true },
    },
    {
        path: "/home",
        component: PersonalPage,
        meta: { needAuth: true },
        children: [
            {
                path: "/",
            },
            {
                path: "/home/my-adverts",
                component: MyAdverts
            },
            {
                path: "/home/create-advert",
                component: CreateAdvert
            },
            {
                path: `/home/edit-advert/:id`,
                component: EditingAdvert
            },
            {
                path: "/home/my-profiles",
                component: MyProfiles
            },
            {
                path: "/home/create-profile",
                component: CreateProfile
            },
            {
                path: "/home/my-replies",
                component: MyReplies
            }
        ]
    },
    {
        path: "/login",
        component: Login,
        meta: { needAuth: false}
    },
    {
        path: "/registration",
        component: Registration,
        meta: { needAuth: false}
    },
    {
        path: "/page-not-found",
        alias: '*',
        component: { render: (h) => h("div", ["404! Page Not Found!"]) },
        meta: { needAuth: false}
    }
]

export default routes