export const UserRole = {
    'Guest': 'Guest',
    'User': 'User',
    'Moderator': 'Moderator'
}